﻿DROP DATABASE IF EXISTS ejemploTrigger;
CREATE DATABASE IF NOT EXISTS ejemploTrigger;
USE ejemploTrigger;

CREATE OR REPLACE TABLE local(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  precio float,
  plazas int,
  PRIMARY KEY(id)
  );

CREATE OR REPLACE TABLE usuario(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  inicial char(1),
  PRIMARY KEY(id)
  );

CREATE OR REPLACE TABLE usa(
  local int,
  usuario int,
  dias int,
  fecha int,
  total float,
  PRIMARY KEY(local,usuario)
  );


/* 1.- Quiero que cuando introduzca un usuario me calcule y almacene la inicial de su nombre */
DELIMITER //
CREATE OR REPLACE TRIGGER usuarioBI
  BEFORE INSERT  
    ON usuario
    FOR EACH ROW
  BEGIN
     SET NEW.inicial=LEFT(NEW.nombre,1);
  END //
DELIMITER ;

/* Realiza parecido al disparador pero sobre toda la tabla */
UPDATE usuario u SET u.inicial=LEFT(u.nombre,1);

TRUNCATE usuario;

INSERT INTO usuario (nombre)
  VALUES ('Roberto'),('ana'),('kevin');

SELECT * FROM usuario u;

ALTER TABLE usuario
  ADD COLUMN contador int DEFAULT 0;

SELECT * FROM usa u;

DELIMITER //
CREATE OR REPLACE TRIGGER usaAI
  AFTER INSERT  
    ON usa
    FOR EACH ROW
  BEGIN
     UPDATE usuario u
        SET contador=contador+1
        WHERE u.id=NEW.usuario;
  END //
DELIMITER ;

TRUNCATE usa;

INSERT usa (local, usuario)
  VALUES (1, 1),(1,2),(2,1);

SELECT * FROM usuario u;
SELECT * FROM usa;


/*
  Consulta de actualizacion uqe realice lo que hace el disparador anterior.
  Me tiene que colocar en contador las veces que ese usuario
  sale en la tabla.
*/
UPDATE 
  usuario u 
  JOIN 
    (SELECT usuario,COUNT(*) cuenta FROM usa u GROUP BY u.usuario) c1 ON u.id=c1.usuario
  SET 
    contador=c1.cuenta;

SELECT usuario,COUNT(*) FROM usa u GROUP BY u.usuario;

SELECT * FROM usuario u;

/*
  Disparador y update que calcule en la tabla local y usa
  el precio * el numero de dias 
*/
