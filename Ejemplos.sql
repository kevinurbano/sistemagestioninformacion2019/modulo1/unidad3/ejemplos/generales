﻿DROP DATABASE IF EXISTS programacion;
CREATE DATABASE IF NOT EXISTS programacion;
USE programacion;

/*
  Ejemplos de procedimientos almacenados  
*/

/*
  P1- Crear un procedimiento almacenado
  muestra la fecha de hoy  
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE p1()
  BEGIN
    SELECT NOW();
  END //
DELIMITER ;

CALL p1();


/*
  p2
  crear una variable de tipo datetime
  y almacenar en esa variable la fecha de hoy.
  Nos muestre la variable
*/

DELIMITER //
CREATE OR REPLACE PROCEDURE p2 ()
  BEGIN
    -- Declarar 
    DECLARE v datetime;
    -- Input (inicializar)
    SET v = NOW();
    -- Output  
    SELECT v;
  END //
DELIMITER ;

CALL p2();

SELECT nombre INTO @v FROM alumnos LIMIT 1;


/* p3 - SUMA 2 NUMEROS
        Que recibe dos numeros y mostrar la suma de ellos */
DELIMITER //
  CREATE OR REPLACE PROCEDURE p3 (num1 int, num2 int)
    BEGIN
      DECLARE suma int DEFAULT 0;
      SET suma = num1 + num2;
      SELECT suma;
    END //
DELIMITER ;

CALL p3(1,5);
CALL p3(88,2);

/* p4 - RESTAR 2 NUMEROS
        Que recibe como argumento */
DELIMITER //
  CREATE OR REPLACE PROCEDURE p4(num1 int, num2 int)
    BEGIN
      DECLARE resta int DEFAULT 0;
      set resta = num1 - num2;
      SELECT resta;  
    END //
DELIMITER ;

CALL p4(1,5);
CALL p4(88,2);

/* 
  P5 - Crear un procedimiento que sume dos numeros y los
       almacene en una tabla llamada datos.
       La tabla la debe crear en casi de que no exista. 
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE p5 (num1 int, num2 int)
  BEGIN
    -- Declarar e inicializar las variables
    DECLARE suma int DEFAULT 0;
    
    -- Crear la tabla
    CREATE TABLE IF NOT EXISTS datos(
        id int AUTO_INCREMENT,
        d1 int DEFAULT 0,
        d2 int DEFAULT 0,
        suma int DEFAULT 0,
        PRIMARY KEY(id)
      );
    
    -- Realizar los calculos
    SET suma = num1+num2;

    -- Almacenar el resultado en la tabla
    INSERT INTO datos VALUES (DEFAULT, num1, num2, suma);

  END //
DELIMITER ;

CALL p5(10,45);
CALL p5(1,56);
SELECT * FROM datos;

/* P6   
   SUMA, producto 2 NUMEROS 
   call p6(14,5)
   Crear una tabla llamada sumaproducto(id,d1,d2,suma,producto) 
   introducir los datos en la tabla y los resultados
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE p6(num1 int, num2 int)
    BEGIN
      -- Crear Variable 
      DECLARE vsuma int DEFAULT 0;
      DECLARE vproducto int DEFAULT 0;
      
      -- Crear la tabla
      CREATE TABLE IF NOT EXISTS sumaproducto(
        id int AUTO_INCREMENT,
        d1 int DEFAULT 0,
        d2 int DEFAULT 0,
        suma int DEFAULT 0,
        producto int DEFAULT 0,
        PRIMARY KEY(id)
      );
      
      -- Realizar Operacion
      SET vsuma = num1 + num2;
      SET vproducto = num1 * num2;

      INSERT INTO sumaproducto VALUES (DEFAULT,num1,num2,vsuma,vproducto);

    END //
DELIMITER ;

CALL p6(20,3);

SELECT * FROM sumaproducto s;

/* P7   
   Sacar la potencia pasando 2 atributos 
*/
DELIMITER //

CREATE OR REPLACE PROCEDURE p7(base int , exponente int)
  BEGIN
    DECLARE resultado int DEFAULT 0;
    SET resultado = POW(base,exponente); -- potencia
      
    CREATE TABLE IF NOT EXISTS potencia (
      id int AUTO_INCREMENT,
      b int,
      e int,
      r int,  
      PRIMARY KEY(id)
    ); 

    INSERT INTO potencia (b, e, r) -- campos
    VALUES (base,exponente,resultado); -- variables
  END //

DELIMITER ;

CALL p7 (2,3);
SELECT * FROM potencia;

/* p8  
   realizar la raiz cuadrada de un numero
   Almacenar el numero y su raiz en la tabla raiz
*/
DELIMITER //

CREATE OR REPLACE PROCEDURE p8(numero1 int)
  BEGIN
    DECLARE resultado float DEFAULT 0;
    
    SET resultado=SQRT(numero1); -- Raiz cuadrada
    
    CREATE TABLE IF NOT EXISTS raiz(
      id int AUTO_INCREMENT,
      n int,      
      r float,
      PRIMARY KEY(id)
    );
    
    INSERT INTO raiz (n,r) VALUES (numero1,resultado);

  END //
DELIMITER ;

CALL p8(9);

SELECT * FROM raiz;

/**
  p9
  Analiza el siguiente procedimiento y añadir lo necesario
  para que calcule la longitud del texto y lo almacene en la tabla
  en un campo llamado longitud
**/
DELIMITER //
DROP PROCEDURE IF EXISTS p9 //
CREATE PROCEDURE p9(argumento varchar(50))
  BEGIN 
    DECLARE l int DEFAULT 0;
    set l = CHAR_LENGTH(argumento);
    
    CREATE TABLE IF NOT EXISTS texto(
      id int AUTO_INCREMENT,
      texto varchar(50),
      longitud int,
      PRIMARY KEY(id)
      );

    INSERT INTO texto(texto,longitud) VALUE (argumento,l);
  END //

DELIMITER ;

CALL p9("Ejemplo");
SELECT * FROM texto;

/**
  CONTROL DE FLUJO, INSTRUCCIONES DE SELECCION  
**/

/**
  sintaxis basica del if
  IF (numero>10) THEN
    sentencias verdadero;
  ELSE
    sentencias falso;
  END IF;
**/

/**
  p10
  Procedimiento al cual le paso un número y me indica si es mayor que 100.
**/
DELIMITER //
  CREATE OR REPLACE PROCEDURE p10(numero int)
    BEGIN

      IF (numero > 100) THEN
        SELECT 'Mayor de 100'; 
      END IF;
    
    END //
DELIMITER ;

CALL p10(120);
CALL p10(90);

/**
  p11
  Procedimiento  al cual le pasas un numero y me indica si es mayor, igual o menor que 100  
**/
DELIMITER //
CREATE OR REPLACE PROCEDURE p11 (numero int)
  BEGIN
    DECLARE resultado varchar(50) DEFAULT "igual a 100";

    IF (numero>100) THEN
      SET resultado = 'Mayor que 100';
    ELSEIF (numero<100) THEN
      SET resultado = 'Menor que 100';
    END IF;

    SELECT resultado;
  END //

DELIMITER ;

CALL p11(90);
CALL p11(110);
CALL p11(100);


/*
  p12  
*/

DELIMITER //
  CREATE OR REPLACE PROCEDURE p12(n int)
    BEGIN
      DECLARE r varchar(50) DEFAULT "Suspenso";
      IF (n>=5 AND n<7) THEN
        SET r="Suficiente";
      ELSEIF (n>=7 AND n<9) THEN
        SET r="Notable";        
      ELSEIF (n>=9) THEN
        SET r="Sobresaliente";        
      END IF;
      SELECT r;
    END //
DELIMITER ;

CALL p12(9);

-- Modificado para CASE
/* 
  p13
  Realizar el mismo ejercicio anterior pero con CASE 
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE p13(n int)
    BEGIN
      DECLARE r varchar(50);    
      CASE
        WHEN (n>=5 AND n<=7) THEN
          SET r='Suficiente';
        WHEN (n>=7 AND n<9) THEN
          SET r='Notable';
        WHEN (n>=9) THEN
          SET r='Sobresaliente';
        ELSE
          SET r='Suspenso';
      END CASE;
      SELECT r;
    END //
DELIMITER ;

CALL p13(8);

-- argumentos de Entrada/Salida

/**
  p14
  Quiero calcular la suma de dos numeros. 
  La suma la almacena en un argumento de salida llmado resultaod.
  call p14(n1 int, n2 int,OUT resultado int);
**/
DELIMITER //
  CREATE OR REPLACE PROCEDURE p14(n1 int, n2 int,OUT resultado int)
    BEGIN
      SET resultado=n1+n2;
    END //
DELIMITER ;

SET @suma=0;
CALL p14(2,5,@suma);
SELECT @suma;

/**
  p15
  Procedimiento que recibe como argumentos.
  n1: numero (argumento de entrada)
  n2: numero (argumento de entrada)
  resultado: numero (argumento salida o entrada/salida)
  calculo; texto (argumento de entrada)
  si calculo vale suma entonces resultado tendra la suma de n1+n2  
  si calculo vale producto entonces resultado tendra el producto de n1+n2
**/
DELIMITER //
  CREATE OR REPLACE PROCEDURE p15(n1 int, n2 int,OUT resultado int, calculo varchar(20))
    BEGIN
      CASE (calculo)
        WHEN 'Suma' THEN
          SET resultado=n1+n2;
        WHEN 'Producto' THEN
          SET resultado=n1*n2;
      END CASE;
    END //
DELIMITER ;

CALL p15(6,2,@r,'suma');
CALL p15(6,2,@r,'producto');
SELECT @r;

/*
  p16
  Utilizando while con este procedimiento vamos a mostrar en pantalla numeros desde el 1 al 10
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE p16()
    BEGIN
      DECLARE contador int DEFAULT 1;
        
      WHILE (contador<=10) DO          
        SELECT contador;
        SET contador=contador+1;        
      END WHILE;
    END //
DELIMITER ;

CALL p16();

/* p17
   Hacer los mismo del ejercicio anterior pero guardar en una tabla y luego mostrarlo*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE p17()
    BEGIN
      DECLARE contador int DEFAULT 1;
      
      CREATE OR REPLACE TEMPORARY TABLE p17(
        id int AUTO_INCREMENT,
        contador int,
        PRIMARY KEY(id)
      );  
     
      WHILE (contador<=10) DO          
        INSERT INTO p17(contador) VALUES (contador);
        SET contador=contador+1;        
      END WHILE;

    END //
DELIMITER ;

CALL p17();

SELECT contador FROM p17 p;


/*
  p18
  Procedimiento que le pasas un numero y te muestra 
  desde el numero 1 hasta el numero pasado
  call p18(5) 1,2,3,4,5 utilizar una tabla temporarl realizarlo con While
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE p18(n int)
    BEGIN
      DECLARE contador int DEFAULT 1;

      CREATE OR REPLACE TEMPORARY TABLE p18(
        id int AUTO_INCREMENT,
        n int,
        PRIMARY KEY(id)
      );

      WHILE(contador<=n) DO 
        INSERT INTO p18 (n) VALUES (contador);
        SET contador=contador+1;
      END WHILE;
      
    END //
DELIMITER ;

CALL p18(3);
SELECT n FROM p18 p;

/**
  F1
  le paso dos numeros y me devuelve la suma de ellos  
**/
DELIMITER //
  CREATE FUNCTION f1(n1 int,n2 int)
    RETURNS int
  BEGIN
    DECLARE resultado int DEFAULT 0;
    SET resultado=n1+n2;  
    RETURN resultado;
  END //
DELIMITER ;

SELECT f1(5,7);