﻿USE programacion;

CREATE OR REPLACE TABLE salas(
  id int AUTO_INCREMENT PRIMARY KEY,
  butacas int,  
  fecha date,
  edad int DEFAULT 0
  );

CREATE OR REPLACE TABLE ventas(
  id int AUTO_INCREMENT PRIMARY KEY,
  sala int,
  numero int DEFAULT 0
  );


/*  
  salaT1:
  Controla la insercion de registros nuevos 
  Disparador para que me Calcule la edad 
  de la sala en funcion de su fecha de alta */

DELIMITER //
CREATE OR REPLACE TRIGGER salaBI
BEFORE INSERT 
  ON salas
  FOR EACH ROW
BEGIN
  SET new.edad=TIMESTAMPDIFF(year,new.fecha,NOW());
  /* UPDATE salas s. set s.edad=TIMESTAMPDIFF(year,fecha,NOW()); */
END //

DELIMITER ;

/* Introduzco registro para comprobar  mi disparador */

INSERT INTO salas (butacas,fecha)
  VALUES (50,'2000/1/1');
SELECT * FROM salas s;

/*
  salasBU
  Controla la actualizacion registros
  Disparador para que me calcule la edad de la sala en
  funcion de su fecha de alta
*/
DELIMITER //
CREATE OR REPLACE TRIGGER salaBU
BEFORE UPDATE 
  ON salas
  FOR EACH ROW
BEGIN
  IF(NEW.fecha<>OLD.fecha) THEN
    SET NEW.edad=TIMESTAMPDIFF(year,NEW.fecha,NOW());  
  END IF;
END //
DELIMITER ;

/* Comprobando el funcionamiento del disparador */

UPDATE salas s SET s.fecha='1995/03/08' WHERE id=1;

/*  Comprobamos los dos disparadores */

INSERT INTO salas (butacas,fecha)
  VALUES (56,'2005/4/8'),(23,'1989/5/5');

SELECT * FROM salas s;

UPDATE salas s SET s.fecha='1990/1/1';

UPDATE 
  salas s 
    SET s.fecha='1995/03/08' WHERE id=1;

/* 
   Necesito que la tabla salas disponga de tres campos nuevos.
   
   Esos campos seran dia,mes,anio 
    
   Quiero que esos tres campos AUTOMATICAMENTE tengan el dia, mes y año de la fecha
  
   Necesito un disparador para insertar y otro para actualizar
*/

ALTER TABLE salas 
  ADD COLUMN IF NOT EXISTS dia int,
  ADD COLUMN IF NOT EXISTS mes int,
  ADD COLUMN IF NOT EXISTS anio int;

DELIMITER //
CREATE OR REPLACE TRIGGER salaBI
BEFORE INSERT 
  ON salas
  FOR EACH ROW
BEGIN
  SET new.edad=TIMESTAMPDIFF(year,new.fecha,NOW());
  SET new.dia=DAY(new.fecha);
  SET new.mes=MONTH(new.fecha);
  SET new.anio=year(new.fecha);
  /* UPDATE salas s. set s.edad=TIMESTAMPDIFF(year,fecha,NOW()); */
END //

DELIMITER ;

INSERT INTO salas (butacas,fecha)
  VALUES (56,'2005/4/8'),(23,'1989/5/5');

SELECT * FROM salas s;

DELIMITER //
CREATE OR REPLACE TRIGGER salaBU
BEFORE UPDATE 
  ON salas
  FOR EACH ROW
BEGIN
 
  SET new.edad=TIMESTAMPDIFF(year,new.fecha,NOW());
  SET new.dia=DAY(new.fecha);
  SET new.mes=MONTH(new.fecha);
  SET new.anio=year(new.fecha);
  
END //
DELIMITER ;


UPDATE salas s SET s.fecha='1990/1/1';

UPDATE salas s SET edad=10;
SELECT * FROM salas s;


CREATE OR REPLACE TABLE ventas1(
  id int AUTO_INCREMENT,
  fecha date,
  precioUnitario float,
  unidad int,
  precioFinal float,
  PRIMARY KEY(id) 
  );


DELIMITER //

CREATE OR REPLACE TRIGGER ventas1BI
BEFORE INSERT 
  ON ventas1
  FOR EACH ROW
BEGIN
  SET NEW.precioFinal=new.unidad * new.precioUnitario;      
END //

DELIMITER ;

INSERT INTO ventas1 (fecha, precioUnitario, unidad)
  VALUES ('1998/2/2', 10.30, 5),
         ('1978/1/5', 20.80, 7),
         ('1948/2/2', 8.10, 10);

SELECT * FROM ventas1 v;
DELIMITER //

CREATE OR REPLACE TRIGGER ventas1BU
BEFORE UPDATE  
  ON ventas1
  FOR EACH ROW
BEGIN
  SET NEW.precioFinal=new.unidad * new.precioUnitario;      
END //

DELIMITER ;

UPDATE 
  ventas1 v
    SET v.unidad=10 WHERE v.id=2;

SELECT * FROM ventas1 v;